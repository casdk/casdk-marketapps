CustomApplicationsHandler.register("app.marketapps", new CustomApplication({

    /**
     * (require)
     *
     * An object array that defines resources to be loaded such as javascript's, css's, images, etc
     *
     * All resources are relative to the applications root path
     */

    require: {

        /**
         * (js) defines javascript includes
         */

        js: [],

        /**
         * (css) defines css includes
         */

        css: ['app.css'],

        /**
         * (images) defines images that are being preloaded
         *
         * Images are assigned to an id
         */

        images: {},

    },

    settings: {
        title: 'Market Apps',
        statusbar: true,
        statusbarIcon: true,
        statusbarTitle: false,
        hasLeftButton: false,
        hasMenuCaret: false,
        hasRightArc: true,

    },

    created: function() {

        // let's build our interface

        // 1) create our context aware sections
        this.createSections();

        // 2) create our statusbar
        this.statusBar = $("<div/>").addClass("status").appendTo(this.canvas);

    },

    /***
     *** Events
     ***/

    /**
     * (event) onControllerEvent
     *
     * Called when a new (multi)controller event is available
     */

    onControllerEvent: function(eventId) {
        // We only get not processed values from the multicontroller here
        switch(eventId) {
          case "selectStart":
             aioWs(message,5);
             console.log(message);
             break;
        }
    },


    /**
     * (event) onContextEvent
     *
     * Called when the context of an element was changed
     */
    onContextEvent: function(eventId, context, element) {

                switch(eventId) {
                    case "selectStart":
                        alert("OKOKO");
                        break;
                    case "upStart":
                        alert("upStart");
                        break;
                    case "ccw":
                        alert("ccw");
                        break;
                    case "cw":
                        alert("cw");
                        break;
                    case "leftStart":
                        alert("leftStart");
                        break;
                    case "rightStart":
                        alert("rightStart");
                        break;
                    case "downStart":
                        alert("downStart");
                        break;
                    case "focused":

                     switch(element.selector) {
                      case '[contextIndex=0]':
                       message="opkg update\n";
                       prev=1;
                       break;
                      case '[contextIndex=1]':
                       message="opkg upgrade\n";
                       prev=2;
                       break;
                      case '[contextIndex=2]':
                       message="opkg list_installed\n";
                       prev=3;
                       break;
                      case '[contextIndex=3]':
                       message="opkg list\n";
                       prev=4;
                       break;
                     }

                     if (prev>0) {
                       aioWs(message,5)
                       console.log(message);
                     }
                     prev=0;
                     break;
                }

        // We only get not processed values from the multicontroller here
    },


    /***
     *** Applicaton specific methods
     ***/

    /**
     * (createSections)
     *
     * This method registers all the sections we want to display
     */

    createSections: function() {

        // random data for testing
        [

            {top: 100, left: 200, title: "Update"},
            {top: 100, left: 450, title: "Upgrade"},
            {top: 250, left: 200, title: "Installed"},
            {top: 250, left: 450, title: "App List"}

        ].forEach(function(item) {
            this.addContext($("<div/>").addClass("section").css(item).append(item.title).appendTo(this.canvas), function(event, element) {
            });

        }.bind(this));


    },


}));

var prev=0;

function aioWs(action, waitMessage) {
  var msgnum = -1;
  var ws = new WebSocket('ws://127.0.0.1:9998/');

  var focusBtn = $('button.selectedItem');
  ws.onmessage = function(event) {
    var res = event.data;
    console.log(res);
    if (res.indexOf('AIO_FC_') !== -1) {
      AioFileCheck(res);
      ws.close();
      return;
    } else if (res.indexOf('DONE') === -1) {
      focusBtn.css({ 'background': '-o-linear-gradient(top,rgba(255,0,0,0),rgba(255,0,000,1))' });
    }
    msgnum++;
    if (msgnum > waitMessage || res.indexOf('DONE') !== -1) {
      focusBtn.css({ 'background': '-o-linear-gradient(top,rgba(0,0,0,0),rgba(0,0,0,1))', 'color': '#fff' });
      setTimeout(function() {
        if (ws !== null) {
          ws.close();
          ws = null;
        }
        $('button').css({ 'background': '' });
      }, 4000);
    }
  };

  ws.onopen = function() {
    ws.send(action);
    focusBtn.css({ 'background': '-o-linear-gradient(top,rgba(255,255,255,.5),rgba(255,255,255,1))', 'color': '#000' });
    //console.info(action);
    if (waitMessage < 1) {
      setTimeout(function() {
        if (ws !== null) {
          ws.close();
          ws = null;
        }
      }, 4000);
    }
  };
  ws.onclose = function() {
    $('button').css({ 'background': '', 'color': '' });
  };
}
